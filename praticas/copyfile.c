#define BUF_SIZE 4096
#define OUTPUT_MODE 0600
int main( int argc, char **argv){
  int inF, outF, rd_count, wt_count;
  char buffer[BUF_SIZE];
  if(argc != 3 ) exit(1);
  if((inF=open(argv[1],O_RDONLY))<0) exit(2);
  if((outF=creat(argv[2],OUTPUT_MODE))<0)exit(3);
  while( 1 ) {
    rd_count = read(inF,buffer,BUF_SIZE);
    if( rd_count <=0 ) break;
    wt_count = write(outF, buffer, rd_count);
    if(wt_count <=0) exit(4);
  }
  close(inF);   close(outF);
  if(rd_count==0) exit(0);
  else exit(5);
} 
