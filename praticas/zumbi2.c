#include<errno.h>
#include<signal.h>
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>

int main(){
  int pid;
  printf("\nProcesso pai, PID=%d\nCriando processo filho...\n", getpid() );
  printf("Entrando em laco infinito.\n");
  pid = fork();
  if(pid==-1){
    perror("Impossivel criar processo filho"); exit(-1);
  } else if( pid == 0 ){
    printf("Sou o processo filho, PID=%d.\nIndo dormir.\n",getpid());
    printf("Use ps -l e confira meu PID e meu estado (S=sleep).");
    printf("O PID e o estado (R=running) do meu pai\nVolto logo\n");
    sleep(60);
    printf("Voltei\nEssa nao, agora sou zumbi");
    exit(0);
  }else{
   // while(1);
  }
}
